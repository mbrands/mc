package de.mbrands.minecraftWeb.server;

import java.util.List;

import de.mbrands.minecraftWeb.client.model.ClientEntity;

public interface IPermissionDataProvider {
	
	List<ClientEntity> getGroupList();
	
	ClientEntity getGroup(int id);
	
	boolean setGroup(ClientEntity group);  
	
	List<ClientEntity> getUserList();
	
	ClientEntity getUser(int id);
	
	boolean setUser(ClientEntity user);  

}
