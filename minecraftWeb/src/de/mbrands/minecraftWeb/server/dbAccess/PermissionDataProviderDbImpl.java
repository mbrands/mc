package de.mbrands.minecraftWeb.server.dbAccess;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import de.mbrands.minecraftWeb.client.model.ClientEntity;
import de.mbrands.minecraftWeb.server.IPermissionDataProvider;
import de.mbrands.minecraftWeb.server.dbAccess.PermissionEntity;

public class PermissionDataProviderDbImpl implements IPermissionDataProvider {
	
	private static final String PERSISTENCE_UNIT_NAME = "permissions";
	private static EntityManagerFactory factory;
	private EntityManager entityManager;
	
	

	public PermissionDataProviderDbImpl() {
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
	     entityManager = factory.createEntityManager();
	}

	@Override
	public List<ClientEntity> getGroupList() {
	    Query query = entityManager.createQuery("select p from PermissionEntity p where p.type = 0");
	    @SuppressWarnings("unchecked")
		List<PermissionEntity> permissionEntityList = query.getResultList();	    
	    List<ClientEntity> entities = new ArrayList<ClientEntity>();
	    for (PermissionEntity permissionEntity : permissionEntityList) {
	    	  entities.add(permissionEntity.toClientEntity());
	    }
		return entities;
	}

	@Override
	public ClientEntity getGroup(int id) {
		PermissionEntity permissionEntity = entityManager.find(PermissionEntity.class, id);		
		return permissionEntity.toClientEntity();
	}

	@Override
	public boolean setGroup(ClientEntity group) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<ClientEntity> getUserList() {
	    Query query = entityManager.createQuery("select p from PermissionEntity p where p.type = 1");
	    @SuppressWarnings("unchecked")
		List<PermissionEntity> permissionEntityList = query.getResultList();	    
	    List<ClientEntity> entities = new ArrayList<ClientEntity>();
	    int i =0;
	    for (PermissionEntity permissionEntity : permissionEntityList) {
	    	System.out.println(i+permissionEntity.getName());
	    	i++;
	    	  entities.add(permissionEntity.toClientEntity());
	    }
		return entities;
	}

	@Override
	public ClientEntity getUser(int id) {
		PermissionEntity permissionEntity = entityManager.find(PermissionEntity.class, id);		
		return permissionEntity.toClientEntity();
	}

	@Override
	public boolean setUser(ClientEntity user) {
		// TODO Auto-generated method stub
		return false;
	}

}
