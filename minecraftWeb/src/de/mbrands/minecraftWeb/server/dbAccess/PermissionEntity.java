package de.mbrands.minecraftWeb.server.dbAccess;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import de.mbrands.minecraftWeb.client.model.ClientEntity;



@Entity
@Table(name="permissions_entity")
public class PermissionEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private int type;
	private String prefix;
	private String suffix;
	
	protected ClientEntity toClientEntity(){
		ClientEntity clientEntity = new ClientEntity(this.getId(), this.getName(), this.getPrefix(), this.getSuffix());
		return clientEntity;		
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}


}
