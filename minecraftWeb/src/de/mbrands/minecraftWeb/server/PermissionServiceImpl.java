package de.mbrands.minecraftWeb.server;

import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import de.mbrands.minecraftWeb.client.model.ClientEntity;
import de.mbrands.minecraftWeb.client.service.PermissionService;
import de.mbrands.minecraftWeb.server.dbAccess.PermissionDataProviderDbImpl;

public class PermissionServiceImpl extends RemoteServiceServlet implements PermissionService{

	private static final long serialVersionUID = 1L;
	private IPermissionDataProvider dataProvider;

	public PermissionServiceImpl() {
		this.dataProvider = new PermissionDataProviderDbImpl();
	}

	@Override
	public List<ClientEntity> getGroupList() {

		return this.dataProvider.getGroupList();
	}

	@Override
	public ClientEntity getGroup(int id) {
		return this.dataProvider.getGroup(id);
	}

	@Override
	public List<ClientEntity> getUserList() {
		return this.dataProvider.getUserList();
	}

	@Override
	public ClientEntity getUser(int id) {
		return this.dataProvider.getUser(id);
	}

}
