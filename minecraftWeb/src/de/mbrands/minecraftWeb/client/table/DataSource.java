package de.mbrands.minecraftWeb.client.table;

import java.util.ArrayList;
import java.util.List;

import de.mbrands.minecraftWeb.client.model.ClientEntity;

public class DataSource {
	
	  private final List<ClientEntity> entities;
	  private List<String> header;

	  public DataSource(List<ClientEntity> entities) {
	    header = new ArrayList<String>();
	    header.add("Id");
	    header.add("Name");
	    header.add("Prefix");
	    header.add("Suffix");
	    this.entities = entities;
	  }

	  public List<ClientEntity> getEntities() {
	    return entities;
	  }

	  public List<String> getTableHeader() {
	    return header;
	  }

}
