package de.mbrands.minecraftWeb.client.table;

import java.util.List;

import com.google.gwt.user.client.ui.FlexTable;

import de.mbrands.minecraftWeb.client.model.ClientEntity;

public class Table extends FlexTable {
	  DataSource input;

	  public Table(DataSource input) {
	    super();
	    this.setCellPadding(1);
	    this.setCellSpacing(0);
	    this.setWidth("100%");
	    this.setInput(input);
	  }

	  public void setInput(DataSource input) {
	    for (int i = this.getRowCount(); i > 0; i--) {
	      this.removeRow(0);
	    }
	    if (input == null) {
	      return;
	    }

	    int row = 0;
	    List<String> headers = input.getTableHeader();
	    if (headers != null) {
	      int i = 0;
	      for (String string : headers) {
	        this.setText(row, i, string);
	        i++;
	      }
	      row++;
	    }
	    // make the table header look nicer
	    this.getRowFormatter().addStyleName(0, "tableHeader");

	    List<ClientEntity> rows = input.getEntities();
	    int i = 1;
	    for (ClientEntity group : rows) {
	    	System.out.println("mbtest table" + i);
	      this.setText(i, 0, Integer.toString(group.getId()));
	      this.setText(i, 1, group.getName());
	      this.setText(i, 2, group.getPrefix());
	      this.setText(i, 3, group.getSuffix());
	      i++;
	    }
	    this.input = input;
	  }

}
