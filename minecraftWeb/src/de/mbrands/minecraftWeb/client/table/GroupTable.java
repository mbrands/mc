package de.mbrands.minecraftWeb.client.table;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class GroupTable extends Composite {

	private static GroupTableUiBinder uiBinder = GWT
			.create(GroupTableUiBinder.class);

	@UiTemplate("EntityTable.ui.xml")
	interface GroupTableUiBinder extends UiBinder<Widget, GroupTable> {
	}

	public GroupTable() {
		initWidget(uiBinder.createAndBindUi(this));
	}

}
