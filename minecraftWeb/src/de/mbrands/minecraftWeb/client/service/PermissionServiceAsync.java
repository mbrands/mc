package de.mbrands.minecraftWeb.client.service;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import de.mbrands.minecraftWeb.client.model.ClientEntity;

public interface PermissionServiceAsync {	
	
	void getGroup(int id, AsyncCallback<ClientEntity> callback);

	void getGroupList(AsyncCallback<List<ClientEntity>> callback);

	void getUser(int id, AsyncCallback<ClientEntity> callback);

	void getUserList(AsyncCallback<List<ClientEntity>> callback);

}
