package de.mbrands.minecraftWeb.client.service;

import java.util.List;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import de.mbrands.minecraftWeb.client.model.ClientEntity;
import de.mbrands.minecraftWeb.client.table.DataSource;
import de.mbrands.minecraftWeb.client.table.Table;

public class EntityCallback implements AsyncCallback<List<ClientEntity>> {

	private Table table;

	public EntityCallback(Table table) {
		this.table = table;
	}

	public void onFailure(Throwable caught) {
		Window.alert(caught.getMessage());

	}

	public void onSuccess(List<ClientEntity> result) {
		List<ClientEntity> entities = result;
		DataSource dataSource = new DataSource(entities);
		table.setInput(dataSource);
		for (ClientEntity entity : entities) {
			System.out.println("mbtest EntityCallback " + entity.getName());
		}

	}

}
