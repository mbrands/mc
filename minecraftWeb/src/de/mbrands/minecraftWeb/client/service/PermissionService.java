package de.mbrands.minecraftWeb.client.service;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import de.mbrands.minecraftWeb.client.model.ClientEntity;

@RemoteServiceRelativePath("permissionService")
public interface PermissionService extends RemoteService {
	
	List<ClientEntity> getGroupList();
	
	ClientEntity getGroup(int id);
	
	List<ClientEntity> getUserList();
	
	ClientEntity getUser(int id);

}
