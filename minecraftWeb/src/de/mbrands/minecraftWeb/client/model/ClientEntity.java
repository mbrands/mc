package de.mbrands.minecraftWeb.client.model;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ClientEntity implements IsSerializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String prefix;
	private String suffix;
	
	public ClientEntity(){};
	
	public ClientEntity(int id, String name, String prefix, String suffix) {
		super();
		this.id = id;
		this.name = name;
		this.prefix = prefix;
		this.suffix = suffix;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
