package de.mbrands.minecraftWeb.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.mbrands.minecraftWeb.client.model.ClientEntity;
import de.mbrands.minecraftWeb.client.service.EntityCallback;
import de.mbrands.minecraftWeb.client.service.PermissionService;
import de.mbrands.minecraftWeb.client.service.PermissionServiceAsync;
import de.mbrands.minecraftWeb.client.table.Table;

public class MinecraftWeb implements EntryPoint {
	
	public void onModuleLoad() {
		
		DockLayoutPanel mainPanel = new DockLayoutPanel(Unit.EM);	
		mainPanel.addNorth(new HTML("<h1>Bastelserver Remote Administration & Investigation Network (BRAIN)</h1>"), 5);
		
		
		TabLayoutPanel mainMenu = new TabLayoutPanel(1.5, Unit.EM);
		mainMenu.add(new HTML("Willkommen"), "Home");
		mainMenu.add(new PermissionsEditor(), "Permissions");
		
		mainPanel.addNorth(mainMenu, 100);
		
		RootLayoutPanel.get().add(mainPanel);

		
	}

	   
	
	

//	 private Table table;
//
//	  
//	 /**
//	    * This is the entry point method.
//	    */
//
//	   public void onModuleLoad() {
//	     table = new Table(null);
//
//	     Button userButton = new Button("User");
//	     Button groupButton = new Button("Groups");
//
//	     // We can add style names
//	     userButton.addStyleName("pc-template-btn");
//	     groupButton.addStyleName("pc-template-btn");
//	     // or we can set an id on a specific element for styling
//
//	     VerticalPanel vPanel = new VerticalPanel();
//	     vPanel.setWidth("100%");
//	     vPanel.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
//	     vPanel.add(userButton);
//	     vPanel.add(groupButton);
//
//	     vPanel.add(table);
//
//	     // add table and button to the RootPanel
//	     RootPanel.get().add(vPanel);
//
//	     // create the dialog box
//	     final DialogBox dialogBox = new DialogBox();
//	     dialogBox.setText("Welcome to GWT Server Communication!");
//	     dialogBox.setAnimationEnabled(true);
//	     Button closeButton = new Button("close");
//	     VerticalPanel dialogVPanel = new VerticalPanel();
//	     dialogVPanel.setWidth("100%");
//	     dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
//	     dialogVPanel.add(closeButton);
//
//	     closeButton.addClickHandler(new ClickHandler() {
//	       @Override
//	       public void onClick(ClickEvent event) {
//	         dialogBox.hide();
//	       }
//	     });
//
//	     // Set the contents of the Widget
//	     dialogBox.setWidget(dialogVPanel);
//
//	     userButton.addClickHandler(new ClickHandler() {
//	       @Override
//	       public void onClick(ClickEvent event) {
//	         PermissionServiceAsync service = (PermissionServiceAsync) GWT
//	             .create(PermissionService.class);
//	         ServiceDefTarget serviceDef = (ServiceDefTarget) service;
//	         serviceDef.setServiceEntryPoint(GWT.getModuleBaseURL()
//	             + "permissionService");
//	         EntityCallback entityCallback = new EntityCallback(table);
//	         service.getUserList(entityCallback);
//	       }
//	     });
//	     
//	     groupButton.addClickHandler(new ClickHandler() {
//		       @Override
//		       public void onClick(ClickEvent event) {
//		         PermissionServiceAsync service = (PermissionServiceAsync) GWT
//		             .create(PermissionService.class);
//		         ServiceDefTarget serviceDef = (ServiceDefTarget) service;
//		         serviceDef.setServiceEntryPoint(GWT.getModuleBaseURL()
//		             + "permissionService");
//		         EntityCallback entityCallback = new EntityCallback(table);
//		         service.getGroupList(entityCallback);
//		       }
//		     });
//}
}
