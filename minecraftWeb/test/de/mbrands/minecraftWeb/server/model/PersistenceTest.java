package de.mbrands.minecraftWeb.server.model;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.mbrands.minecraftWeb.client.model.ClientEntity;
import de.mbrands.minecraftWeb.server.IPermissionDataProvider;
import de.mbrands.minecraftWeb.server.dbAccess.PermissionDataProviderDbImpl;

public class PersistenceTest {
	private IPermissionDataProvider dataProvider;


	@Before
	public void setUp() throws Exception {
		this.dataProvider = new PermissionDataProviderDbImpl();
	}


	@Test
	public void getGroupListTest() {
		List<ClientEntity> groupList = dataProvider.getGroupList();
		assertNotNull("Groups successfully read", groupList.get(1).getName());
	}
	
	@Test
	public void getGroupTest() {
		ClientEntity group = dataProvider.getGroup(1);
		assertNotNull("Group successfully read", group.getName());
	}
	
	@Test
	public void getUserListTest() {
		List<ClientEntity> userList = dataProvider.getUserList();
		assertNotNull("Users successfully read", userList.get(1).getName());
	}
	
	@Test
	public void getUserTest() {
		ClientEntity user = dataProvider.getUser(1);
		assertNotNull("User successfully read", user.getName());
	}

}
